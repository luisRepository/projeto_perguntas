import 'package:flutter/material.dart';
import 'package:projeto_perguntas/questao.dart';
import 'package:projeto_perguntas/questionario.dart';
import 'package:projeto_perguntas/resposta.dart';
import 'package:projeto_perguntas/resultado.dart';

void main() {
  runApp(PerguntaApp());
}

class PerguntaApp extends StatefulWidget {
  PerguntaApp({Key? key}) : super(key: key);

  @override
  State<PerguntaApp> createState() => _PerguntaAppState();
}

class _PerguntaAppState extends State<PerguntaApp> {
  var _perguntaSelecionada = 0;
  var _pontuacaoTotal = 0;

  final List<Map<String, Object>> perguntas = [
    {
      'texto': 'Qual é a sua cor favorita?',
      'respostas': [
        {'texto': 'Preto', 'nota': 2},
        {'texto': 'Vermelho', 'nota': 5},
        {'texto': 'Verde', 'nota': 7},
        {'texto': 'Amarelo', 'nota': 10},
      ]
    },
    {
      'texto': 'Qual é o seu animal favorito?',
      'respostas': [
        {'texto': 'Cachorro', 'nota': 2},
        {'texto': 'Gato', 'nota': 5},
        {'texto': 'Coelho', 'nota': 7},
        {'texto': 'Hamster', 'nota': 10},
      ]
    }
  ];

  void _responder(int pontuacao) {
    print(pontuacao);
    if (temPerguntaSelecionada)
      setState(() {
        _perguntaSelecionada++;
        _pontuacaoTotal += pontuacao;
      });
  }

  void _reiniciarQuestionario(){
    setState(() {
      _perguntaSelecionada = 0;
      _pontuacaoTotal = 0;
    });
  }

  bool get temPerguntaSelecionada {
    return _perguntaSelecionada < perguntas.length;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Perguntas'),
            centerTitle: true,
            backgroundColor: Colors.black,
          ),
          body: temPerguntaSelecionada
              ? Questionario(perguntas, _perguntaSelecionada, _responder)
              : Resultado(_pontuacaoTotal, _reiniciarQuestionario)),
    );
  }
}
