import 'package:flutter/material.dart';
import 'package:projeto_perguntas/questao.dart';
import 'package:projeto_perguntas/resposta.dart';

class Questionario extends StatelessWidget {
  final List<Map<String, Object>> perguntas;
  final int index;
  final void Function(int) _responder;

  const Questionario(this.perguntas, this.index, this._responder);

  bool get temPerguntaSelecionada {
    return index < perguntas.length;
  }

  @override
  Widget build(BuildContext context) {
    List<Map<String, Object>> respostas =
        temPerguntaSelecionada ? perguntas[index].cast()['respostas'] : [];

    return Column(
      children: [
        Questao(perguntas[index]['texto'].toString()),
        SizedBox(
          height: 20,
        ),
        ...respostas.map((resp) {
          return Resposta(
              resp['texto'].toString(), () => _responder(int.parse(resp['nota'].toString())));
        })
      ],
    );
  }
}
